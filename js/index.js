$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });
    $("#suscribete").on('show.bs.modal', function(e) {
        console.log('se esta mostrando el modal');
        $("#activa-modal1-2").removeClass("btn-outline-success");
        $("#activa-modal1-2").addClass("btn-dark");
        $("#activa-modal1-2").prop("disabled", true);
    });
    $("#suscribete").on("shown.bs.modal", function(e) {
        console.log('ya se mostro el modal');
    });
    $("#suscribete").on('hide.bs.modal', function(e) {
        console.log('se esta ocultando el modal');
        $('#activa-modal1-2').removeClass("btn-dark");
        $('#activa-modal1-2').addClass("btn-outline-success");
        $('#activa-modal1-2').prop('disabled', false);
    });
    $("#suscribete").on('hidden.bs.modal', function(e) {
        console.log('ya se oculto el modal');
    });
});